import sys
import os
import subprocess
import commands
import unittest

class TestMath(unittest.TestCase):

    def setUp(self):
        print("Starting the setup")

    def tearDown(self):
        print("Executing Teardown")

    def testAdd(self):
        status, output = commands.getstatusoutput("../a.out 2 a 2")
        self.assertEqual(status, 0)
        self.assertEqual(output, "The result is :4")

    def testSubtract(self):
        status, output = commands.getstatusoutput("../a.out 20 s 2")
        self.assertEqual(status, 0)
        self.assertEqual(output, "The result is :18")

    def testMultiply(self):
        status, output = commands.getstatusoutput("../a.out 2 m 3")
        self.assertEqual(status, 0)
        self.assertEqual(output, "The result is :6")

    def testDivide(self):
        status, output = commands.getstatusoutput("../a.out 20 d 2")
        self.assertEqual(status, 0)
        self.assertEqual(output, "The result is :1")

if __name__ == "__main__":
    unittest.main(verbosity=2)
