#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char *argv[])
{
    if(argc != 4)
    {
        printf("Please give command line arguments in the format below:\n");
        printf("num1 m num2 for multiplication\n");
        printf("num1 d num2 for division\n");
        printf("num1 s num2 for subraction\n");
        printf("num1 a num2 for addition\n");
        printf("Where num1 and num2 are integers\n");
    }
    int arg1 = atoi(argv[1]);
    int arg2 = atoi(argv[3]);
    char *operator = argv[2];
    if(strcmp(operator, "m") == 0)
    {
        int mult = arg1 * arg2;
        printf("The result is :%d\n", mult);
    }
    else if(strcmp(operator, "d") == 0)
    {
        int div = arg1 / arg2;
        printf("The result is :%d\n", div);
    }
    else if(strcmp(operator, "a") == 0)
    {
        int sum = arg1 + arg2;
        printf("The result is :%d\n", sum);
    }
    else if(strcmp(operator, "s") == 0)
    {
        int sub = arg1 - arg2;
        printf("The result is :%d\n", sub);
    }
    else
        printf("Not a valid operator\n");
    
    return (0);
}
